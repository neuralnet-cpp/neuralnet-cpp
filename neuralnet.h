/**
 * NeuralNet - 2013-04-22 - https://gitlab.com/neuralnet-cpp
 *
 *   A C++ neural network that aims to be as simple and lightweight as possible.
 *
 * Copyright (C) 2013-2015 Martin Haggstrom <https://gitlab.com/Mhn>
 *
 * Released under the Creative Commons Attribution-ShareAlike License
 *   (http://creativecommons.org/licenses/by-sa/3.0/)
 *
 **/

#ifndef NEURALNET_CPP_H
#define NEURALNET_CPP_H

#include <random>
#include <cmath>

inline double sigmoid(double x)
{
    return 1.0/(1.0 + std::exp(-x));
}

inline double sigmoidDerivate(double x)
{
    return x * (1.0 - x);
}

typedef double(*Callback)(double);

template<size_t... PreviousLayerSizes> class NeuralNet;

template<size_t LayerSize>
class NeuralNet<LayerSize>
{
protected:
    NeuralNet() {}

    inline void feedForward(const double* input, const Callback)
    { m_outvalues = input; }

    inline double getOutput(size_t index) const
    {
        return index >= m_size ? 1.0 : m_outvalues[index];
    }

protected:
    inline void _backPropagate(const Callback) {}
    inline void _updateWeights(double, double) {}

    static const size_t m_size = LayerSize;
    const double* m_outvalues;
    double m_gradientvalues[m_size + 1];
};

template<size_t LayerSize, size_t... PreviousLayerSizes>
class NeuralNet<LayerSize, PreviousLayerSizes...> :
        public NeuralNet<PreviousLayerSizes...>
{
    typedef NeuralNet<PreviousLayerSizes...> PreviousLayer;
public:
    NeuralNet()
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(0.0, 1.0);
        for (size_t i = 0; i < m_size; ++i)
        {
            for (size_t j = 0; j <= PreviousLayer::m_size; ++j)
            {
                m_weights[i][j] = dis(gen);
                m_deltaWeights[i][j] = 0.0;
            }
        }
    }

    inline double getOutput(size_t index) const
    {
        // The NN has one "virtual" extra node at each level that is always 1.0
        return index < m_size ? m_outvalues[index] : 1.0;
    }

    inline void feedForward(const double* input, const Callback f = sigmoid)
    {
        PreviousLayer::feedForward(input, f);

        for (size_t i = 0; i < m_size; i++)
        {
            m_invalues[i] = 0.0;
            for (size_t j = 0; j <= PreviousLayer::m_size; j++)
            {
                m_invalues[i] += m_weights[i][j] * PreviousLayer::getOutput(j);
            }
            m_outvalues[i] = f(m_invalues[i]);
        }
    }

    void backPropagate(const double* output, double eta,
                       double alpha, Callback f = sigmoidDerivate)
    {
        for (size_t i = 0; i < m_size; i++)
        {
            m_gradientvalues[i] = f(m_outvalues[i]) * (output[i] - m_outvalues[i]);
        }

        _backPropagate(f);
        _updateWeights(eta, alpha);
    }

    // RMS - root mean square error
    void calculateRMS(const double* output, int window = 1000)
    {
        double error = 0.0;
        for (size_t i = 0; i < m_size; i++)
        {
            error += (m_outvalues[i] - output[i]) * (m_outvalues[i] - output[i]);
        }
        error = std::sqrt(error / m_size);

        m_averageError = (m_averageError * window + error) / (window + 1.0);
    }

    double getRecentError()   const { return m_averageError; }
    const double* getResult() const { return m_outvalues;    }

protected:
    inline void _backPropagate(const Callback f)
    {
        for (size_t j = 0; j <= PreviousLayer::m_size; j++)
        {
            double sum = 0.0;
            for (size_t i = 0; i < m_size; i++)
            {
                sum += m_gradientvalues[i] * m_weights[i][j];
            }
            PreviousLayer::m_gradientvalues[j] = f(PreviousLayer::getOutput(j)) * sum;
        }
        PreviousLayer::_backPropagate(f);
    }

    inline void _updateWeights(double eta, double alpha)
    {
        for (size_t i = 0; i < m_size; i++)
        {
            for (size_t j = 0; j <= PreviousLayer::m_size; j++)
            {
                double oldDelta = m_deltaWeights[i][j];
                m_deltaWeights[i][j] =
                        eta * PreviousLayer::getOutput(j) * m_gradientvalues[i] +
                        alpha * oldDelta;
                m_weights[i][j] += m_deltaWeights[i][j];
            }
        }
        PreviousLayer::_updateWeights(eta, alpha);
    }

public:
    static const size_t m_size = LayerSize;

protected:
    double m_averageError = 0.0;
    double m_weights       [m_size][PreviousLayer::m_size + 1];
    double m_deltaWeights  [m_size][PreviousLayer::m_size + 1];
    double m_invalues      [m_size];
    double m_outvalues     [m_size];
    double m_gradientvalues[m_size + 1];
};

#endif // NEURALNET_CPP_H
