#include "neuralnet.h"

#include <random>
#include <iostream>

void xor_test()
{
    std::cout << std::endl << "Testing XOR between 2 input values:" << std::endl;

    /**************************************************************************
     * Creates a neural network with a number of layers.
     * Leftmost value is output layer, rightmost value is input layer.
     */
    NeuralNet<1, 5, 5, 2> n;

    double a[2];
    double b[1];

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 1);

    /**************************************************************************
     * Teach the neural network XOR between the two input values.
     */
    for (int i = 0; i < 100000; i++)
    {
        // randomize 0 or 1
        a[0] = dis(gen);
        a[1] = dis(gen);

        // ...or, loop through all combinations (00, 01, 10, 11)
        //a[0] = i % 4 > 1 ? 1 : 0;
        //a[1] = (i % 4) % 2 ? 1 : 0;

        // a[0] XOR a[1]
        b[0] = int(a[0]) ^ int(a[1]);

        // Calculate the values throughout the network with random inputs
        n.feedForward(a);

        // Give the network the expected output, the learning rate,
        // and a momentum. Update weights in the network
        n.backPropagate(b, 0.15, 0.5);

        // Optionally calculate RMS after each back propagation to be able to
        // retrieve it later with getRecentError().
        n.calculateRMS(b);

        // Print root mean square error each 10000:th repetition
        if (i % 10000 == 9999)
            std::cout << i + 1 << " RMS: " << n.getRecentError() << std::endl;
    }

    /**************************************************************************
     * Validate output
     */
    std::cout << "Validation:" << std::endl;
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            a[0] = i;
            a[1] = j;

            b[0] = i ^ j;

            n.feedForward(a);
            std::cout << "Input: [ "             << i << " XOR " << j << " ] "
                      << "Expected result: [ "   << b[0]              << " ] "
                      << "Calculated result: [ " << n.getResult()[0]  << " ] "
                      <<  std::endl;
        }
    }
}

void add_test()
{
    std::cout << std::endl << "Testing ADD between 3 input values:" << std::endl;

    /**************************************************************************
     * Creates a neural network with a number of layers.
     * Leftmost value is output layer, rightmost value is input layer.
     */
    NeuralNet<1, 5, 5, 5, 3> n;

    double a[3];
    double b[1];

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 1);

    /**************************************************************************
     * Teach the neural network addition between the three input values.
     */
    for (int i = 0; i < 100000; i++)
    {
        // randomize 0 or 1
        a[0] = dis(gen);
        a[1] = dis(gen);
        a[2] = dis(gen);

        // The default sigmoid functions in the neural net only support values
        // between 0 and 1, so we must scale any values given to the neural net
        // to that range. Afterwards, we scale the actual results back up again
        // to our original range.
        b[0] = (a[0] + a[1] + a[2]) / 3.0;

        // Calculate the values throughout the network with random inputs
        n.feedForward(a);

        // Give the network the expected output, the learning rate,
        // and a momentum. Update weights in the network
        n.backPropagate(b, 0.15, 0.5);

        // Optionally calculate RMS after each back propagation to be able to
        // retrieve it later with getRecentError().
        n.calculateRMS(b);

        // Print root mean square error each 10000:th repetition
        if (i % 10000 == 9999)
            std::cout << i + 1 << " RMS: " << n.getRecentError() << std::endl;
    }

    /**************************************************************************
     * Validate output
     */
    std::cout << "Validation:" << std::endl;
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            for (int k = 0; k < 2; k++)
            {
                a[0] = i;
                a[1] = j;
                a[2] = k;

                b[0] = double(i + j + k) / 3.0;

                n.feedForward(a);
                std::cout << "Input: [ "        << i << "+" << j << "+" << k   << " ] "
                          << "Expected result: [ "   << b[0] * 3.0             << " ] "
                          << "Calculated result: [ " << n.getResult()[0] * 3.0 << " ] "
                          <<  std::endl;
            }
        }
    }
}

int main(int, char**)
{
    xor_test();

    add_test();

    return 0;
}
